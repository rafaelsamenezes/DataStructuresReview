#include <linked_list.h>
#include <stdlib.h>
void enqueue(LinkedList *ll, int elem) {
  LinkedListNode *node = createLinkedListNodeNode();
  node->data = elem;
  node->next = NULL;
  // Add at the end
  if (ll->head == NULL) {
    ll->head = node;
  } else {
    LinkedListNode *aux = ll->head;
    while (aux->next != NULL) {
      aux = aux->next;
    }
    aux->next = node;
  }
}

#include <assert.h>
// Same implementation as pop
int dequeue(LinkedList *ll) {
  assert(ll->head != NULL);
  int result = ll->head->data;
  LinkedListNode *aux = ll->head;
  ll->head = ll->head->next;
  free(aux);
  return result;
}

LinkedListDriver Queue() {
  LinkedListDriver driver = LinkedListBase();
  driver.insert = enqueue;
  driver.remove = dequeue;
  return driver;
}