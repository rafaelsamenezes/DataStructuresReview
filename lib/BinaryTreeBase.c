#include <bin_tree.h>
#include <container.h>
#include <stdlib.h>

BinTreeNode* new_binary_node(int value, unsigned key) {
  BinTreeNode* node = (BinTreeNode*)malloc(sizeof(BinTreeNode));
  node->left = NULL;
  node->right = NULL;
  node->value = value;
  node->key = key;
}
int max(int a, int b) { return a > b ? a : b; }
int tree_height(BinTreeNode* btree) {
  if (btree == NULL)
    return -1;
  else {
    return 1 + max(tree_height(btree->left), tree_height(btree->right));
  }
}

char is_node_leaf(BinTreeNode* node) {
  return (node->left == NULL) && (node->right == NULL);
}

BinTree* new_binary_tree() {
  BinTree* btree = (BinTree*)malloc(sizeof(BinTree));
  btree->root = NULL;
  return btree;
}

static Container* new_binary_tree_container() {
  Container* c = (Container*)malloc(sizeof(Container));
  c->data = (void*)new_binary_tree();
  c->length = 0;
  return c;
}
static void destroy_node(BinTreeNode* node) {
  if (node == NULL) return;
  if (node->left != NULL) destroy_node(node->left);
  if (node->right != NULL) destroy_node(node->right);
  free(node);
}
void destroy_tree(BinTree* btree) {
  if (btree->root != NULL) destroy_node(btree->root);
  free(btree);
}
static void destroy_container(Container* c) {
  destroy_tree((BinTree*)c->data);
  free(c);
}

static void add_item(Container* c, int value) {
  BinTree* bt = (BinTree*)c->data;
  BinTreeNode* node = new_binary_node(value, c->length++);
  if (bt->root == NULL) {
    bt->root = node;
  } else {
    BinTreeNode* helper = bt->root;
    while (1) {
      if (node->key <= helper->key && helper->left == NULL) {
        helper->left = node;
        break;
      } else if (node->key > helper->key && helper->right == NULL) {
        helper->right = node;
        break;
      }
      helper = node->key < helper->key ? helper->left : helper->right;
    }
  }
}
static int get_item(Container* c, unsigned key) {
  assert(key < c->length);
  BinTree* bt = (BinTree*)c->data;
  BinTreeNode* helper = bt->root;

  while (helper->key != key) {
    assert(helper != NULL);
    helper = key < helper->key ? helper->left : helper->right;
  }
  return helper->value;
}

static void update_references(BinTreeNode* node, unsigned key) {
  if (node == NULL) return;
  if (key >= node->key) {
    if (node->right != NULL) update_references(node->right, key);

  } else {
    if (node->right != NULL) update_references(node->right, key);
    if (node->left != NULL) update_references(node->left, key);

    node->key--;
  }
}

static BinTreeNode* set_sucessor(BinTreeNode* root) {
  assert(root != NULL);
  assert(!is_node_leaf(root));
  // Find rightest of the left son or right son
  if (root->left != NULL) {
    BinTreeNode *temp, *helper = root->left;

    while (helper->right != NULL) {
      temp = helper;
      helper = helper->right;
    }
    temp->right = NULL;
    helper->left = root->left;
    helper->right = root->right;
    free(root);
    return helper;
  } else {
    BinTreeNode* temp = root->right;
    free(root);
    return temp;
  }
}
static void remove_item(Container* c, unsigned key) {
  assert(key < c->length);
  BinTree* bt = (BinTree*)c->data;
  BinTreeNode *temp, *helper = bt->root;

  while (helper->key != key) {
    temp = helper;
    helper = key < helper->key ? helper->left : helper->right;
  }
  if (is_node_leaf(helper)) {
    // Trivial case: Node is a leaf
    free(helper);
    // If is leaf and is root
    if (helper == bt->root) {
      bt->root = NULL;
    } else if (temp->right == helper) {
      temp->right = NULL;
    } else {
      temp->left = NULL;
    }
  } else {
    // Node is not a leaf, so find sucessor
    BinTreeNode* new_root = set_sucessor(helper);
    if (temp->right == helper) {
      temp->right = new_root;
    } else if (temp->left == helper) {
      temp->left = new_root;
    }
    // Update root if needed
    if (helper == bt->root) bt->root = new_root;
  }
  update_references(bt->root, key);
  c->length--;
}

static void set_item(Container* c, unsigned key, int value) {
  assert(key < c->length);
  BinTree* bt = (BinTree*)c->data;
  BinTreeNode* helper = bt->root;

  while (helper->key != key) {
    assert(helper != NULL);
    helper = key < helper->key ? helper->left : helper->right;
  }
  helper->value = value;
}

// TODO: Optimize
static void swap_item(Container* c, unsigned key1, unsigned key2) {
  assert(key1 < c->length);
  assert(key2 < c->length);

  int value1 = get_item(c, key1);
  int value2 = get_item(c, key2);

  set_item(c, key1, value2);
  set_item(c, key2, value1);
}
ContainerDriver BinaryTree() {
  ContainerDriver d;
  d.init = new_binary_tree_container;
  d.destroy = destroy_container;
  d.add_item = add_item;
  d.get_item = get_item;
  d.set_item = set_item;
  d.swap_item = swap_item;
  d.remove_item = remove_item;
  return d;
}
