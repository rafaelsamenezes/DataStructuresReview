#include <heap.h>

inline unsigned heap_parent(unsigned key) { return ((key + 1) / 2) - 1; }
inline unsigned heap_left(unsigned key) { return ((key + 1) * 2) - 1; }
inline unsigned heap_right(unsigned key) { return ((key + 1) * 2); }

void max_heapify(Container *c, ContainerDriver d, unsigned smaller) {
  unsigned left = heap_left(smaller);
  unsigned right = heap_right(smaller);
  unsigned largest = smaller;
  if (left < c->length) {
    if (d.get_item(c, left) > d.get_item(c, smaller)) {
      largest = left;
    }
  }

  if (right < c->length) {
    if (d.get_item(c, right) > d.get_item(c, largest)) {
      largest = right;
    }
  }

  if (largest != smaller) {
    d.swap_item(c, smaller, largest);
    max_heapify(c, d, largest);
  }
}

void build_max_heap(Container *c, ContainerDriver d) {
  unsigned i;
  for (i = c->length / 2; i > 0; i--) {
    max_heapify(c, d, i);
  }
  max_heapify(c, d, 0);
}
