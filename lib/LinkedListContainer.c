#include <container.h>
#include <linked_list.h>
#include <stdlib.h>
#define cast_ll LinkedList *ll = (LinkedList *)data->data
Container *createLinkedListContainer() {
  Container *c = (Container *)malloc(sizeof(Container));
  c->length = 0;
  c->data = LinkedListBase().init();
  return c;
}

void destroyContainer(Container *data) {
  cast_ll;
  LinkedListBase().destroy(ll);
  free(data);
}

void addItem(Container *data, int element) {
  LinkedListNode *node = createLinkedListNodeNode();
  node->data = element;
  node->next = NULL;
  cast_ll;
  // Add at the end
  if (ll->head == NULL) {
    ll->head = node;
  } else {
    LinkedListNode *aux = ll->head;
    while (aux->next != NULL) {
      aux = aux->next;
    }
    aux->next = node;
  }
  data->length++;
}

void removeItem(Container *data, unsigned key) {
  check_length(key, data);
  cast_ll;

  if (key == 0) {
    LinkedListNode *aux = ll->head->next;
    free(ll->head);
    ll->head = aux;
  } else {
    LinkedListNode *aux = ll->head;
    while (key > 1) {
      aux = aux->next;
      key--;
    }
    LinkedListNode *temp = aux->next;
    free(aux->next);
    aux->next = temp;
  }
  data->length--;
}

int getItem(Container *data, unsigned key) {
  check_length(key, data);
  cast_ll;
  LinkedListNode *aux = ll->head;
  while (key > 0) {
    aux = aux->next;
    key--;
  }
  return aux->data;
}

void setItem(Container *data, unsigned key, int new_value) {
  check_length(key, data);
  cast_ll;
  LinkedListNode *aux = ll->head;
  while (key > 0) {
    aux = aux->next;
    key--;
  }
  aux->data = new_value;
}

void swapItem(Container *data, unsigned key1, unsigned key2) {
  int value1 = getItem(data, key1);
  int value2 = getItem(data, key2);
  if (value1 != value2) {
    setItem(data, key1, value2);
    setItem(data, key2, value1);
  }
}

ContainerDriver LinkedListContainer() {
  ContainerDriver driver;
  driver.init = createLinkedListContainer;
  driver.destroy = destroyContainer;
  driver.add_item = addItem;
  driver.remove_item = removeItem;
  driver.get_item = getItem;
  driver.set_item = setItem;
  driver.swap_item = swapItem;

  return driver;
}
