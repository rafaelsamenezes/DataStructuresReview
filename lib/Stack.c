#include <linked_list.h>
#include <stdlib.h>
void push(LinkedList *ll, int elem) {
  LinkedListNode *node = createLinkedListNodeNode();
  node->data = elem;
  node->next = ll->head;
  ll->head = node;
}

#include <assert.h>
int pop(LinkedList *ll) {
  assert(ll->head != NULL);
  int result = ll->head->data;
  LinkedListNode *aux = ll->head->next;
  free(ll->head);
  ll->head = aux;
  return result;
}

LinkedListDriver Stack() {
  LinkedListDriver driver = LinkedListBase();
  driver.insert = push;
  driver.remove = pop;
  return driver;
}