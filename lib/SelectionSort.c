#if !defined(SelectionSort_H)
#define SelectionSort_H
#include <sort.h>

void selection_sort(Container *c, ContainerDriver d) {
  int i = 0;
  for (; i < c->length; i++) {
    unsigned current_min = i;
    int j = i + 1;
    for (; j < c->length; j++) {
      int j_value = d.get_item(c, j);
      if (j_value < d.get_item(c, current_min)) {
        current_min = j;
      }
    }
    d.swap_item(c, i, current_min);
  }
}

SortDriver SelectionSort() {
  SortDriver driver;
  driver.sort = selection_sort;
  return driver;
}
#endif  // SelectionSort_H
