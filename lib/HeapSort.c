#include <container.h>
#include <heap.h>
#include <sort.h>

void heap_sort(Container *c, ContainerDriver d) {
  build_max_heap(c, d);
  unsigned i = 0;

  unsigned default_size = c->length;
  for (i = c->length - 1; i > 1; i--) {
    d.swap_item(c, 0, i);
    c->length--;
    max_heapify(c, d, 0);
  }
  c->length = default_size;
}

SortDriver HeapSort() {
  SortDriver driver;
  driver.sort = heap_sort;
  return driver;
}