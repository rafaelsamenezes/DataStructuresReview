#include <sort.h>
#include <stdio.h>
#include <stdlib.h>

void merge(Container *c, ContainerDriver d, unsigned first, unsigned last) {
  unsigned mid = (first + last) / 2;

  unsigned length = (last - first + 1);
  int *temp = (int *)malloc(sizeof(int) * length);

  unsigned i = 0;
  unsigned i_left = first;
  unsigned i_right = mid + 1;

  while ((i_left <= mid) && (i_right <= last)) {
    int left_value = d.get_item(c, i_left);
    int right_value = d.get_item(c, i_right);

    if (left_value < right_value) {
      temp[i++] = left_value;
      i_left++;
    } else {
      temp[i++] = right_value;
      i_right++;
    }
  }

  // If some sub-array finished before, then we have to copy all the elements
  while (i < length) {
    temp[i++] =
        i_left <= mid ? d.get_item(c, i_left++) : d.get_item(c, i_right++);
  }

  int j = first;
  for (i = 0; i < length; i++) {
    d.set_item(c, j++, temp[i]);
  }

  free(temp);
}

void mergesort_aux(Container *c, ContainerDriver d, unsigned first,
                   unsigned last) {
  if (first < last) {
    unsigned mid = (first + last) / 2;
    mergesort_aux(c, d, first, mid);
    mergesort_aux(c, d, mid + 1, last);
    merge(c, d, first, last);
  }
}

void merge_sort(Container *c, ContainerDriver d) {
  mergesort_aux(c, d, 0, c->length - 1);
}

SortDriver MergeSort() {
  SortDriver driver;
  driver.sort = merge_sort;
  return driver;
}