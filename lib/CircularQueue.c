#include <linked_list.h>
#include <stdlib.h>

void circular_enqueue(LinkedList *ll, int elem) {
  LinkedListNode *node = createLinkedListNodeNode();
  node->data = elem;
  // Add at the end
  // If queue was empty
  if (ll->head == NULL) {
    ll->head = node;
  } else {
    LinkedListNode *aux = ll->head;
    // Find ending
    while (aux->next != ll->head) {
      aux = aux->next;
    }
    aux->next = node;
  }
  // Circular
  node->next = ll->head;
}

void free_circular_queue(LinkedList *ll) {
  if (ll->head != NULL) {
    LinkedListNode *first = ll->head;
    LinkedListNode *aux = ll->head;
    while (aux->next != first) {
      LinkedListNode *temp = aux->next;
      free(aux);
      aux = temp;
    }
  }
  free(ll);
}

#include <assert.h>
int circular_dequeue(LinkedList *ll) {
  assert(ll->head != NULL);
  int result = ll->head->data;
  if (ll->head == ll->head->next) {
    free(ll->head);
    ll->head = NULL;
  } else {
    LinkedListNode *first = ll->head;
    LinkedListNode *aux = ll->head;
    while (aux->next != first) {
      aux = aux->next;
    }
    ll->head = ll->head->next;
    free(first);
    aux->next = ll->head;
  }
  return result;
}

LinkedListDriver CircularQueue() {
  LinkedListDriver driver = Queue();
  driver.insert = circular_enqueue;
  driver.destroy = free_circular_queue;
  driver.remove = circular_dequeue;
  return driver;
}
