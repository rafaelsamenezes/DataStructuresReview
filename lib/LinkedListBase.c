#include <linked_list.h>
#include <stdlib.h>
LinkedListNode *createLinkedListNodeNode() {
  LinkedListNode *node = (LinkedListNode *)malloc(sizeof(LinkedListNode));
  node->data = 0;
  node->next = NULL;
  return node;
}

LinkedList *createLinkedList() {
  LinkedList *ll = (LinkedList *)malloc(sizeof(LinkedList));
  ll->head = NULL;
  return ll;
}

void freeLinkedList(LinkedList *ll) {
  while (ll->head != NULL) {
    LinkedListNode *aux = ll->head->next;
    free(ll->head);
    ll->head = aux;
  }
  free(ll);
}

char isEmpty(LinkedList *ll) { return ll->head == NULL; }

LinkedListDriver LinkedListBase() {
  LinkedListDriver driver;
  driver.init = createLinkedList;
  driver.destroy = freeLinkedList;
  driver.is_empty = isEmpty;
  return driver;
}
