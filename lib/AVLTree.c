#include <bin_tree.h>
#include <container.h>
#include <stdio.h>
#include <stdlib.h>
int balance_factor(BinTree *btree) {
  return tree_height(btree->root->left) - tree_height(btree->root->right);
}

int balance_factor_node(BinTreeNode *node) {
  if (node == NULL) return 0;
  return tree_height(node->left) - tree_height(node->right);
}
char check_if_balanced(BinTreeNode *node, unsigned key) {}

static BinTreeNode *find_ancestor(BinTree *btree, unsigned key) {
  if (btree->root == NULL) {
    return NULL;
  }

  BinTreeNode *helper = btree->root;
  BinTreeNode *grandpa = helper;
  while (1) {
    if (key > helper->key) {
      if (helper->right != NULL) {
        grandpa = helper;
        helper = helper->right;
      } else
        break;
    } else {
      if (helper->left != NULL) {
        grandpa = helper;
        helper = helper->left;
      } else
        break;
    }
  }

  return grandpa;
}

typedef enum avl_insertion_case {
  SIMPLE_LEFT,
  DOUBLE_LEFT,
  SIMPLE_RIGHT,
  DOUBLE_RIGHT
} AVL_INSERTION_CASE;

static AVL_INSERTION_CASE get_insertion_case(int grandpa_bf, int father_bf) {
  if (grandpa_bf < 0)
    return father_bf < 0 ? SIMPLE_LEFT : DOUBLE_LEFT;
  else
    return father_bf > 0 ? SIMPLE_RIGHT : DOUBLE_RIGHT;
}

static void simple_left_rotation(BinTreeNode *grandpa, BinTreeNode *father) {
  printf("Left simple on %u\n", father->key);
  grandpa->right = NULL;
  father->left = grandpa;
}

static void simple_right_rotation(BinTreeNode *grandpa, BinTreeNode *father) {
  printf("Right simple\n");
  grandpa->left = NULL;
  father->right = grandpa;
}

static BinTreeNode *double_left_rotation(BinTreeNode *grandpa,
                                         BinTreeNode *father) {
  printf("Double left simple\n");
  grandpa->right = father->left;
  father->left = NULL;
  grandpa->right->right = father;
  BinTreeNode *new_father = grandpa->right;
  simple_left_rotation(grandpa, grandpa->right);
  return new_father;
}

static BinTreeNode *double_right_rotation(BinTreeNode *grandpa,
                                          BinTreeNode *father) {
  printf("Double right simple\n");
  grandpa->left = father->right;
  father->right = NULL;
  grandpa->left->left = father;
  BinTreeNode *new_father = grandpa->left;
  simple_left_rotation(grandpa, grandpa->left);
  return new_father;
}

static void avl_add_item(Container *c, int value) {
  BinTree *bt = (BinTree *)c->data;
  unsigned key = c->length;
  BinTreeNode *ancestor = find_ancestor(bt, key);
  BinaryTree().add_item(c, value);
  // If ancestor is NULL, the tree was empty. So there is no need for balance
  if (ancestor != NULL) {
    BinTreeNode *father =
        key > ancestor->key ? ancestor->right : ancestor->left;
    /* printf("ANCESTORS: grandpa: %u, father: %u\n", ancestor->key,
     * father->key); */
    int balance_factor_grandpa = balance_factor_node(ancestor);
    printf("\nBF_GRANDPA: %d\n", balance_factor_grandpa);
    int balance_factor_father = balance_factor_node(father);
    printf("BF_FATHER: %d\n", balance_factor_father);
    if (abs(balance_factor_grandpa) > 1) {
      // Tree is unbalaced check which case should be used
      AVL_INSERTION_CASE rotation_case =
          get_insertion_case(balance_factor_grandpa, balance_factor_father);

      switch (rotation_case) {
        case SIMPLE_LEFT: {
          if (bt->root == ancestor) {
            bt->root = father;
          } else {
            BinTreeNode *gran_grandpa = find_ancestor(bt, ancestor->key);
            gran_grandpa->right = father;
          }
          simple_left_rotation(ancestor, father);
          break;
        }
        case SIMPLE_RIGHT: {
          if (bt->root == ancestor) {
            bt->root = father;
          } else {
            BinTreeNode *gran_grandpa = find_ancestor(bt, ancestor->key);
            gran_grandpa->left = father;
          }
          simple_right_rotation(ancestor, father);
          break;
        }
        case DOUBLE_LEFT: {
          BinTreeNode *temp = double_left_rotation(ancestor, father);
          if (bt->root == ancestor) {
            bt->root = temp;
          } else {
            BinTreeNode *gran_grandpa = find_ancestor(bt, ancestor->key);
            gran_grandpa->left = father;
          }
          break;
        }
        case DOUBLE_RIGHT: {
          BinTreeNode *temp = double_right_rotation(ancestor, father);
          if (bt->root == ancestor) {
            bt->root = temp;
          } else {
            BinTreeNode *gran_grandpa = find_ancestor(bt, ancestor->key);
            gran_grandpa->left = father;
          }
          break;
        }
      }
    }
  }
}

ContainerDriver AVLTree() {
  ContainerDriver d = BinaryTree();
  d.add_item = avl_add_item;
  return d;
}
