#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct linked_list_node {
  int data;
  struct linked_list_node *next;
} LinkedListNode;

typedef struct linked_list {
  LinkedListNode *head;
} LinkedList;

typedef struct linked_list_driver {
  LinkedList *(*init)();
  void (*destroy)(LinkedList *ll);
  void (*insert)(LinkedList *ll, int elem);
  int (*remove)(LinkedList *ll);
  char (*is_empty)(LinkedList *ll);
} LinkedListDriver;

LinkedListNode *createLinkedListNodeNode();
LinkedListDriver LinkedListBase();
LinkedListDriver Stack();
LinkedListDriver Queue();
LinkedListDriver CircularQueue();
#endif  // LINKED_LIST_H
