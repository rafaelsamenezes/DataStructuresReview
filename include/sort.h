#include <container.h>
#if !defined(SORT_H)
#define SORT_H

typedef struct sort_driver {
  void (*sort)(Container *container, ContainerDriver driver);
} SortDriver;

SortDriver InsertionSort();
SortDriver SelectionSort();
SortDriver MergeSort();
SortDriver QuickSort();
SortDriver HeapSort();
SortDriver RadixSort();
#endif  // SORT_H
