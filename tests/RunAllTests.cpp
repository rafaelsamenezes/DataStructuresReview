#include "CppUTest/CommandLineTestRunner.h"

IMPORT_TEST_GROUP(ConstArrayTest);
IMPORT_TEST_GROUP(LinkedListContainerTest);
IMPORT_TEST_GROUP(ConstArraySortingTest);
IMPORT_TEST_GROUP(StackTest);

IMPORT_TEST_GROUP(QueueTest);
IMPORT_TEST_GROUP(CircularQueueTest);
IMPORT_TEST_GROUP(DoubleLinkedTest);
IMPORT_TEST_GROUP(BinaryTreeTest);
IMPORT_TEST_GROUP(AVLTreeTest);

int main(int argc, char **argv) { return RUN_ALL_TESTS(argc, argv); }