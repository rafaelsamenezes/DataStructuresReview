#include <CppUTest/MemoryLeakDetectorMallocMacros.h>
#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>
#include "const.h"
extern "C" {
#include <linked_list.h>
}

TEST_GROUP(StackTest) {
  LinkedList *ll = Stack().init();

  void setup() {
    for (size_t i = 0; i < array_size; i++) {
      Stack().insert(ll, array_to_sort[i]);
    }
  }

  void teardown() { Stack().destroy(ll); }
};

TEST(StackTest, PushPopValues) {
  LinkedList *stack = Stack().init();
  int expected = 3;
  Stack().insert(stack, expected);
  int actual = Stack().remove(stack);
  CHECK_EQUAL(expected, actual);
  Stack().destroy(stack);
}

TEST(StackTest, LIFO) {
  for (size_t i = 0; i < array_size; i++) {
    int expected = array_to_sort[array_size - i - 1];
    int actual = Stack().remove(ll);
    CHECK_EQUAL(expected, actual);
  }
}

TEST(StackTest, NotIsEmpty) {
  LinkedList *ll = Stack().init();
  Stack().insert(ll, 2);
  CHECK_FALSE(Stack().is_empty(ll));
  Stack().destroy(ll);
}

TEST(StackTest, AfterRemoveEmpty) {
  LinkedList *ll = Stack().init();

  int i;
  for (i = 0; i < 10; i++) {
    Stack().insert(ll, i);
  }

  for (i = 0; i < 10; i++) {
    Stack().remove(ll);
  }

  CHECK_TRUE(Stack().is_empty(ll));
  Stack().destroy(ll);
}

TEST(StackTest, NotEmpty) {
  LinkedList *ll = Stack().init();
  CHECK_TRUE(Stack().is_empty(ll));
  Stack().destroy(ll);
}

TEST_GROUP(QueueTest) {
  LinkedList *ll = Queue().init();

  void setup() {
    for (size_t i = 0; i < array_size; i++) {
      Queue().insert(ll, array_to_sort[i]);
    }
  }

  void teardown() { Queue().destroy(ll); }
};

TEST(QueueTest, EnqueueDequeueValues) {
  LinkedList *queue = Queue().init();
  int expected = 3;
  Queue().insert(queue, expected);
  int actual = Queue().remove(queue);
  CHECK_EQUAL(expected, actual);
  Queue().destroy(queue);
}

TEST(QueueTest, FIFO) {
  for (size_t i = 0; i < array_size; i++) {
    int expected = array_to_sort[i];
    int actual = Queue().remove(ll);
    CHECK_EQUAL(expected, actual);
  }
}

TEST(QueueTest, NotIsEmpty) {
  LinkedList *queue = Queue().init();
  Queue().insert(queue, 2);
  CHECK_FALSE(Queue().is_empty(queue));
  Queue().destroy(queue);
}

TEST(QueueTest, AfterRemoveEmpty) {
  LinkedList *queue = Queue().init();

  int i;
  for (i = 0; i < 10; i++) {
    Queue().insert(queue, i);
  }

  for (i = 0; i < 10; i++) {
    Queue().remove(queue);
  }

  CHECK_TRUE(Queue().is_empty(queue));
  Queue().destroy(queue);
}

TEST(QueueTest, NotEmpty) {
  LinkedList *queue = Queue().init();
  CHECK_TRUE(Queue().is_empty(queue));
  Queue().destroy(queue);
}

TEST_GROUP(CircularQueueTest) {
  LinkedList *ll = CircularQueue().init();

  void setup() {
    for (size_t i = 0; i < array_size; i++) {
      CircularQueue().insert(ll, array_to_sort[i]);
    }
  }

  void teardown() { CircularQueue().destroy(ll); }
};

TEST(CircularQueueTest, CircularEnqueueDeCircularQueueValues) {
  LinkedList *circularQueue = CircularQueue().init();
  int expected = 3;
  CircularQueue().insert(circularQueue, expected);
  int actual = CircularQueue().remove(circularQueue);
  CHECK_EQUAL(expected, actual);
  CircularQueue().destroy(circularQueue);
}

TEST(CircularQueueTest, FIFO) {
  for (size_t i = 0; i < array_size; i++) {
    int expected = array_to_sort[i];
    int actual = CircularQueue().remove(ll);
    CHECK_EQUAL(expected, actual);
  }
}

TEST(CircularQueueTest, NotIsEmpty) {
  LinkedList *ll = CircularQueue().init();
  CircularQueue().insert(ll, 2);
  CHECK_FALSE(CircularQueue().is_empty(ll));
  CircularQueue().destroy(ll);
}

TEST(CircularQueueTest, AfterRemoveEmpty) {
  LinkedList *ll = CircularQueue().init();

  int i;
  for (i = 0; i < 10; i++) {
    CircularQueue().insert(ll, i);
  }

  for (i = 0; i < 10; i++) {
    CircularQueue().remove(ll);
  }

  CHECK_TRUE(CircularQueue().is_empty(ll));
  CircularQueue().destroy(ll);
}

TEST(CircularQueueTest, NotEmpty) {
  LinkedList *ll = CircularQueue().init();
  CHECK_TRUE(CircularQueue().is_empty(ll));
  CircularQueue().destroy(ll);
}