#include <bin_tree.h>
#include <container.h>
#include <linked_list.h>
#include <sort.h>
#include <stdio.h>
#include <stdlib.h>
extern int balance_factor(BinTree *btree);
int main() {
  Container *test;
  ContainerDriver driver = AVLTree();
  test = driver.init();
  int i = 0;
  int max = 10;
  for (; i < max; i++) {
    printf("START\n");
    printf("i: %d:", i);
    driver.add_item(test, 100 - i);
    printf("END\n");
  }

  printf("START\n");
  for (i = 0; i < max; i++) {
    int value = driver.get_item(test, i);
    printf("Value: %d\n", value);
  }
  printf("END\n");

  BinTree *btree = (BinTree *)test->data;
  /* printf("Current balance value: %d\n", balance_factor(btree)); */
  driver.destroy(test);
  return 0;
}
