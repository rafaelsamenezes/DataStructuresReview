# DataStructuresReview 
[![Build Status](https://travis-ci.org/RafaelSa94/DataStructuresReview.svg?branch=master)](https://travis-ci.org/RafaelSa94/DataStructuresReview)
[![Coverage Status](https://img.shields.io/coveralls/github/RafaelSa94/DataStructuresReview.svg)](https://coveralls.io/github/RafaelSa94/DataStructuresReview)
[![License](https://img.shields.io/github/license/RafaelSa94/DataStructuresReview.svg)](https://github.com/RafaelSa94/DataStructuresReview/blob/master/LICENSE)

This is a sample project to learn more about:

* C programming language
* Data Structures
* Sorting algorithms
* Optimizations
* Testing (CppUTest)
* CMake

Since this project is more didactic than real, I will try to use lots
of comments and only support the integer type (so no void* magic). To
make the sorting algorithms works on different data structures, two api will be used
container.h and sorting.h
