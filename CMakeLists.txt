cmake_minimum_required(VERSION 3.9.2)
project(DataStructures)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake)

option(COVERALLS "Generate coveralls data" OFF)
set(TestSuiteType Full Fast None)
set(TestSuite Fast CACHE STRING "How many tests to build")
set_property(CACHE TestSuite PROPERTY STRINGS ${TestSuiteType})

if(NOT TestSuite IN_LIST TestSuiteType)
    message(FATAL_ERROR "TestSuite must be one of ${TestSuiteType}")
endif()

if (COVERALLS)
    include(Coveralls)
    coveralls_turn_on_coverage()
endif()
include_directories(include)
add_subdirectory(lib)

if (COVERALLS)
    set(COVERAGE_SRCS lib/CircularQueue.c lib/ConstArray.c lib/DoubleLinkedList.c lib/Heap.c
        lib/HeapSort.c lib/InsertionSort.c lib/LinkedListBase.c lib/LinkedListContainer.c 
        lib/MergeSort.c lib/Queue.c lib/QuickSort.c lib/RadixSort.c lib/SelectionSort.c lib/Stack.c    
    )

    # Create the coveralls target.
    coveralls_setup(
        "${COVERAGE_SRCS}" # The source files.
        ON)                # If we should upload.

endif()
if (NOT (TestSuite STREQUAL None))
    enable_testing()
    add_subdirectory(tests)

    
endif()

